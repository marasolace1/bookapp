// let list = document.getElementsByTagName('li') // [l1,li,li,li] - HTMLElements - Not a valid js Array
// let list2 = document.querySelectorAll('li') // [l1,li,li,li] - NodeList - is a valid JS Array
// let content = document.querySelector('ul')
const btn = document.querySelector('button')
const list = document.querySelector('ul')
const deleteItem = document.getElementsByClassName('delete')
const inputItem = document.getElementById('input')
const listItem = document.createElement('li')
const addBook = document.getElementsByClassName('add-book')[0]
const hideBox = document.querySelector('#hide')
const icon = document.querySelector('.icon')



  // listItem.childElement.classList.addAtrribute("class","delete")

const storedItem = JSON.parse(localStorage.getItem('books')) || []


// addEventListeners()


// function addEventListeners(){
//   btn.addEventListener('click', addBookName)
 
// }

const displayItems = () => {
    storedItem.reverse().map((item) => {
      return list.innerHTML += "<li>" + "<span class ='name'>"+ item + "</span>" + "<span class ='delete'>Delete</span> </li>"
})
}
displayItems()


//add book names to local storage

// function addBookName(){
  
//     if(inputItem.value === ""){
//       alert ('Please add a book of your choice')
//       }
//       else{
//         storedItem.push(inputItem.value)
//       }

      // localStorage.setItem('books', JSON.stringify(storedItem))
      // displayItems()
//       inputItem.value = ""
// }


//add book names but not to local storage
addBook.addEventListener('submit', (e) => {
  e.preventDefault()
  const textInput = addBook.querySelector('input[type="text"]').value
  // console.log(textInput);
  const li = document.createElement('li')
  const deletebtn = document.createElement('span')
  li.textContent = textInput
  deletebtn.className = "delete"
  deletebtn.textContent = "delete"
  li.appendChild(deletebtn)
  // const books = list.getElementsByTagName('li')
 
  const books = list.getElementsByTagName('li')
  Array.from(books).forEach((book) => {
    const title = book.textContent
    if( textInput === ""){
      alert ('Please enter a book name!')
      throw new Error("Please enter a book name")
    }else if(title.includes(textInput.toLowerCase())){
      alert ('book name already exist!')
      inputItem.value =""
      throw new Error("Ouch book name exists already")
  }else{
    inputItem.value =""
    return;
  }
  })
  // list.appendChild(li)
  storedItem.push(textInput)
  localStorage.setItem('books', JSON.stringify(storedItem))  
  list.prepend(li) // adds list to the top

  e.preventDefault()
  

})

// alternatively
// function addBookName(e){
//   e.preventDefault()
//   const value = inputItem.value
// const li = document.createElement('li')
// const deletebtn = document.createElement('span')
// li.textContent = textInput + " - "
// deletebtn.textContent = "delete"

// li.appendChild(deletebtn)
// list.appendChild(li)

// }


//Deleting items from list
list.addEventListener('click', (e) => {
  if(e.target.className === 'delete'){
    const deleteList = e.target.parentElement
    const itemName = e.target.parentElement.querySelector('.name').textContent
    
      const index = storedItem.indexOf(itemName);
      if (index > -1) {
        storedItem.splice(index, 1);
      }
      list.removeChild(deleteList) 
    return localStorage.setItem('books', JSON.stringify(storedItem))  
  
  }
})


//alternatively
// convert li to an array before deleting
// Array.from(deleteItem).forEach(function(item){
//   item.addEventListener('click', function(e){
//     const deleteList = e.target.parentElement
//     deleteList.parentElement.removeChild(deleteList)
    
//   })
// })


// using checkboxes to hide and reveal items
hideBox.addEventListener('change', (e) => {
  if(hideBox.checked){
    list.style.display = "none"
  } else {
    list.style.display = "initial"
  }
})


// filter and search
const searchBar = document.forms[0]
searchBar.addEventListener('keyup', (e) => {
  const word = e.target.value.toLowerCase()
  const books = list.getElementsByTagName('li')
  Array.from(books).forEach(function(book){
    const title = book.textContent
    if(title.toLowerCase().indexOf(word) != -1){
    book.style.display ='block'
  } else {
    book.style.display = "none"
  }
  })
  e.preventDefault()
})
// console.log(searchBar);


// icon.addEventListener('click', (e) => {
//   Array.from(icon.children).forEach(item => {
//     if(item.className === "grid"){
//       item.classList.add(' active')
//     }else{
//       item.classList.remove('active')
//     }
//   })
  
// })